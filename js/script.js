const menuButtons = $('button[data-menu]')

menuButtons.click(function () {
    const menu = $(this).data('menu')

    $('.menu[data-menu]').removeClass('open')
    $(`.menu[data-menu="${menu}"]`).addClass('open')

    menuButtons.removeClass('btn-active')
    $(this).addClass('btn-active')
});


$(document).ready(function(){
    let owl = $(".owl-carousel");

    owl.owlCarousel({
        items: 1,
        // loop: true // Безкінечний цикл каруселі
    });


    function updateSlideInfo(event) {
        let current = event.item.index + 1;
        let next = current === event.item.count ? 1 : current + 1;
        let prev = current === 1 ? event.item.count : current - 1;

        $('.current-slide').text(current);
        $('.next-slide').text(next);
        $('.prev-slide').text(prev);
    }


    owl.on('initialized.owl.carousel', function(event) {
        updateSlideInfo(event);
    });


    owl.on('changed.owl.carousel', function(event) {
        updateSlideInfo(event);
    });


    $('.go-to-first').click(function() {
        owl.trigger('to.owl.carousel', [0, 300]);
    });

    $('.go-to-last').click(function() {
        let lastItemIndex = owl.find('.owl-item').length - 1;
        owl.trigger('to.owl.carousel', [lastItemIndex, 300]);
    });
});

